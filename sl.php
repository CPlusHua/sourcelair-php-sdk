<?php
/*
Copyright (C) 2012 sourceLair

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and 
associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons 
to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

class slApp {
	
	private $appid;
	private $baseURL = 'https://api.sourcelair.com';
	const C = 1;
	const CPP = 2;
	const ObjectiveC = 3;
	const Fortran = 6;
	const Pascal = 7;
	const Python = 8;
	const Ruby = 13;
	const Lua = 14;
	
	public $responseInfo;
	
	function __construct( $appid = null ) {
		
		$this -> appid = $appid;
		
	}
	
	public function run ( $code = null , $language = null , $input = null ) {
		
		return $this -> request( 'exec' , 'create' , array(
		
			'code'		=> $code ,
			'language'	=> $language
		
		));
		
	}
	
	public function request( $resource = null , $method = null , $id = null , $data = null ) {
		
		if ( !$this -> appid ) throw new Exception( 'No AppID exists.' );
		
		if ( !$resource ) throw new Exception( 'No resource given.' );
		
		if ( !$method ) throw new Exception( 'No method given.' );
		
		$url = $this -> baseURL . '/' . $resource . '/' . $method;
	
		if ( is_array( $id ) ) {
				
			$data = $id;
			$id = null;
		
		}
		
		if ( $id ) $url .= '/' . $id;
		
		$url .= '/?app=' . $this -> appid;
		
		$method = strtolower( $method );
		
		$req_methods = array(
		
			'view'		=> 'get' ,
			'list'		=> 'get' ,
			'create'	=> 'post' ,
			'update'	=> 'post' ,
			'delete'	=> 'post'
		
		);

		if ( !isset( $req_methods[ $method ] ) ) throw new Exception( 'This method does not exist.' );
		
		$request = curl_init(); 
		
		if ( $req_methods[ $method ] == 'post' ) {
		
			curl_setopt( $request , CURLOPT_POST , true );
			curl_setopt( $request , CURLOPT_POSTFIELDS , $data );
			
		}else{
			
			$getValues = "";
			
			foreach ( $data as $key => $value ) {
				
				$getValues .= '&' . urlencode( $key ) . '=' . urlencode( $value );
				
			}
			
			$url .= $getValues;
			
		}
		
		curl_setopt( $request , CURLOPT_RETURNTRANSFER , 1 );
		curl_setopt( $request , CURLOPT_URL , $url );
		curl_setopt( $request , CURLOPT_FAILONERROR , false );
		
		$response = curl_exec( $request );
		
		$this -> responseInfo = curl_getinfo( $request );
		
		if ( $this -> responseInfo[ 'http_code' ] == 400 ) throw new Exception( $response );
		
		if ( is_array( json_decode( $response , true ) ) ) $response = json_decode( $response , true );
		
		return $response;
		
	}
	
}

?>